---
layout: page
title: About
hide_hero: true
---

`opentps_flash` is an expansion of OpenTPS that enables the optimization of 3D range modulators for conformal FLASH proton therapy.

## License
`opentps_flash` is released under the Apache-2.0 licence.

