---
layout: page
title: Docs
hide_hero: true
---

`opentps_flash` is an expansion of OpenTPS that enables the optimization of 3D range modulators for conformal FLASH proton therapy.

The range modulator optimization algorithm is comprehensively described in the following reference: [Optimization of patient-specific range modulators for conformal FLASH proton therapy](https://doi.org/10.48550/arXiv.2303.08649).

Multiple dose rate definitions have been implemented, and a review of these definitions can be accessed via the following link: [Definition of FLASH dose rate in pencil beam scanning: a comparative study](https://doi.org/10.48550/arXiv.2303.02056).

To enhance the dose rate locally, the scanning pattern can be optimized using the algorithm described in detail in this reference: [Optimization of pencil beam scanning pattern for FLASH proton therapy](https://doi.org/10.48550/arXiv.2304.05721).

Please explore the example directory within the [opentps_flash repository](https://gitlab.com/flash-tps/flash-tps) for further insights.

