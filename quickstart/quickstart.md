---
layout: page
title: Quickstart
hide_hero: true
show_sidebar: true
---


`opentps_flash` is an extension of OpenTPS that requires the prior installation of OpenTPS and proper configuration of your environment. For instructions on how to obtain OpenTPS and configure your environment, please refer to http://opentps.org/.

## Download the code
```
git clone https://gitlab.com/flash-tps/opentps_flash.git
```

